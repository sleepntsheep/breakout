#ifndef SHEEP_PROFILER_H
#define SHEEP_PROFILER_H

#include <assert.h>
#include <stdint.h>
#include <stddef.h>
#include <time.h>

struct profile {
    const char *name;
    clock_t total;
    clock_t start;
};

struct profiler {
    struct profile *profiles;
    size_t len;
    struct profile *latest;
};

void profiler_init(struct profiler *profiler);
void profiler_cleanup(struct profiler *profiler);
void profiler_start(struct profiler *profiler, const char *name);
void profiler_stop(struct profiler *profiler);

#endif /* SHEEP_PROFILER_H */

#ifdef SHEEP_PROFILER_IMPLEMENTATION

#include <string.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#define profiler_foreach(iter, counter, profiler, block) \
    for (size_t counter = 0; counter < (profiler)->len; counter++) \
    { \
        struct profile *iter = (profiler)->profiles + counter; \
        block \
    }

void profile_start(struct profile *profile)
{
    profile->start = clock();
}

void profile_stop(struct profile *profile)
{
    profile->total += clock() - profile->start;
    profile->start = 0L;
}

struct profile *profiler_find(struct profiler *profiler, const char *name)
{
    struct profile *target = NULL;
    profiler_foreach(prof, i, profiler, {
        if (strcmp(name, prof->name) == 0)
        {
            target = prof;
        }
    });
    return target;
}

struct profile *profiler_find_create(struct profiler *profiler, const char *name)
{
    if (profiler->profiles == NULL)
    {
        profiler->len = 0;
        profiler->profiles = malloc(sizeof *profiler->profiles);
    }
    struct profile *target = profiler_find(profiler, name);
    if (target)
    {
        return target;
    }
    else
    {
        profiler->profiles = realloc(profiler->profiles, sizeof *profiler->profiles * (profiler->len + 1));
        profiler->profiles[profiler->len++] = (struct profile) {
            .name = name,
        };
        return profiler->profiles + profiler->len - 1;
    }
}

void profiler_cleanup(struct profiler *profiler)
{
    if (profiler->profiles == NULL)
        return;
    free(profiler->profiles);
}

void profiler_start(struct profiler *profiler, const char *name)
{
    struct profile *target = profiler_find_create(profiler, name);
    profile_start(target);
    profiler->latest = target;
}

void profiler_stop(struct profiler *profiler)
{
    assert(profiler->latest);
    profile_stop(profiler->latest);
}

void profiler_print(struct profiler *profiler)
{
    if (profiler->profiles == NULL)
        return;
    int max_name_len = 0;
    uintmax_t total_clock = 0;
    int max_clock_len = 0;
    profiler_foreach(prof, i, profiler, {
        int nlen = strlen(prof->name);
        if (max_name_len < nlen) max_name_len = nlen;
        {
            clock_t tmp = prof->total;
            int tlen = 0;
            for (clock_t tmp = prof->total; tmp >= 1; tlen++)
            {
                tmp /= 10;
            }
            if (max_clock_len < tlen) max_clock_len = tlen;
        }
        total_clock += prof->total;
    })
    profiler_foreach(prof, i, profiler, {
        printf("%*s: %*.0lf -- %2.2lf%\n", max_name_len, prof->name, max_clock_len,
                (double)prof->total, 100 * prof->total / (double)total_clock);
    })
}

#endif /* SHEEP_PROFILER_IMPLEMENTATION */

