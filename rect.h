#ifndef RECT_H_
#define RECT_H_

typedef struct {
    int x, y, w, h;
} Rect;

#define Rect(x, y, w, h) ((Rect){x, y, w, h})

#ifndef MIN
#define MIN(a,b) ((a) < (b) ? (a) : (b))
#endif
#ifndef MAX
#define MAX(a,b) ((a) > (b) ? (a) : (b))
#endif
#ifndef ABS
#define ABS(a) ((a) < 0 ? (-(a)) : (a))
#endif

int RectIntersect(Rect a, Rect b);
int RectIntersectCartesian(Rect a, Rect b);

#endif /* RECT_H_ */

#ifdef RECT_IMPLEMENTATION

int RectIntersect(Rect a, Rect b)
{
    int left = MAX(a.x, b.x);
    int right = MIN(a.x + a.w, b.x + b.w);
    int top = MAX(a.y, b.y);
    int bottom = MIN(a.y + a.h, b.y + b.h);
    if (left >= right || top >= bottom)
    {
        return 0;
    }
    return (right - left) * (bottom - top);
}

int RectIntersectCartesian(Rect a, Rect b)
{
    /* cartesian plane instead of topleft-as-0-0 plane */
    /* TODOOO: implement this? */
    return 0;
}

#endif /* RECT_IMPLEMENTATION */

