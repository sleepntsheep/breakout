#include <X11/Xatom.h>
#include <X11/Xlib.h>
#include <X11/Xproto.h>
#include <X11/Xutil.h>
#include <X11/keysymdef.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#define SHEEP_RENDERER_IMPLEMENTATION
#include "renderer.h"
#define RECT_IMPLEMENTATION
#include "rect.h"
#define SHEEP_PROFILER_IMPLEMENTATION
#include "profiler.h"

/* types and enums */
enum wm_atoms {
    wm_delete,
    wm_atoms_count,
};

/* constants */
#define FPS 60
#define WIDTH 800
#define HEIGHT 600
#define BALL_SIZE 20
#define BALL_SPEED 0.4
#define HANDLE_WIDTH 100
#define HANDLE_HEIGHT 20
#define HANDLE_SPEED 0.8
#define BLOCK_WIDTH 30
#define BLOCK_HEIGHT 20
#define BLOCK_NROW 4
#define BLOCK_NCOL 15
#define BLOCK_PADDING 20
#define BLOCK_X_OFFSET                                                         \
    ((WIDTH - BLOCK_NCOL * BLOCK_WIDTH - (BLOCK_NCOL - 1) * BLOCK_PADDING) / 2)
#define BLOCK_Y_OFFSET 100
#define BLOCK_X(col) ((col)*BLOCK_WIDTH + (col)*BLOCK_PADDING + BLOCK_X_OFFSET)
#define BLOCK_Y(row) ((row)*BLOCK_HEIGHT + (row)*BLOCK_PADDING + BLOCK_Y_OFFSET)

/* variables */
static bool block_is_removed[BLOCK_NROW][BLOCK_NCOL] = {0};
static Display *dpy = NULL;
static Window win;
static Atom wm_atoms[wm_atoms_count];
static GC gc = 0;
static Visual *visual = NULL;
static Renderer rend = {0};
static double ball_x = 0;
static double ball_y = 500;
static double ball_vx = BALL_SPEED;
static double ball_vy = -BALL_SPEED;
static double handle_x = 300;
static const int handle_y = 500;
static bool key_cache[1 << 8] = {0};
static struct profiler prof = {0};
static int depth;

/* non-constant macros */
#define HANDLE_RECT Rect(handle_x, handle_y, HANDLE_WIDTH, HANDLE_HEIGHT)
#define BALL_RECT Rect(ball_x, ball_y, BALL_SIZE, BALL_SIZE)

/* functions */
void init(void);
void cleanup(void);
void frame(void);
void update(void);

int main() {
    init();

    for (;;) {
        profiler_start(&prof, "event");
        while (XPending(dpy) > 0) {
            XEvent event;
            XNextEvent(dpy, &event);
            switch (event.type) {
            case KeyPress: {
                char c = XLookupKeysym(&event.xkey, 0);
                switch (c) {
                case 'q':
                    goto end;
                default:
                    break;
                }
                if (c >= 0) {
                    key_cache[c] = true;
                }
            } break;
            case KeyRelease: {
                char c = XLookupKeysym(&event.xkey, 0);
                if (c >= 0) {
                    key_cache[c] = false;
                }
            }
            case ClientMessage:
                if ((Atom)event.xclient.data.l[0] == wm_atoms[wm_delete])
                    goto end;
                break;
            default:
                break;
            }
        }
        profiler_stop(&prof);

        update();
        frame();

        profiler_start(&prof, "draw to X11 window");
        XImage *image =
            XCreateImage(dpy, visual, depth, ZPixmap, 0, (char *)rend.fb, WIDTH,
                         HEIGHT, 32, sizeof *rend.fb * WIDTH);
        XPutImage(dpy, win, gc, image, 0, 0, 0, 0, WIDTH, HEIGHT);
        free(image);
        profiler_stop(&prof);

        profiler_start(&prof, "frame cap");
        //usleep(1000000L / 60);
        profiler_stop(&prof);
    }

end:
    cleanup();
    return EXIT_SUCCESS;
}

void init(void) {
    profiler_start(&prof, "Init");
    dpy = XOpenDisplay(NULL);
    win = XCreateSimpleWindow(dpy, XDefaultRootWindow(dpy), 100, 100, 200, 200,
                              4, 0, 0xFFFFFF);
    XMapWindow(dpy, win);
    wm_atoms[wm_delete] = XInternAtom(dpy, "WM_DELETE_WINDOW", false);
    XSetWMProtocols(dpy, win, wm_atoms + wm_delete, 1);
    XSetWMNormalHints(dpy, win,
                      &(XSizeHints){
                          .flags = PMinSize | PMaxSize,
                          .min_width = WIDTH,
                          .max_width = WIDTH,
                          .min_height = HEIGHT,
                          .max_height = HEIGHT,
                      });
    XSelectInput(dpy, win, KeyPressMask | ExposureMask | KeyReleaseMask);
    XStoreName(dpy, win, "Breakout!");
    XSync(dpy, false);
    gc =
        XCreateGC(dpy, win, GCBackground, &(XGCValues){.background = 0x000000});
    visual = XDefaultVisual(dpy, DefaultScreen(dpy));
    renderer_init(&rend, WIDTH, HEIGHT);
    depth = XDefaultDepth(dpy, XDefaultScreen(dpy));
    profiler_stop(&prof);
}

void cleanup(void) {
    profiler_start(&prof, "Cleanup");
    XDestroyWindow(dpy, win);
    XFreeGC(dpy, gc);
    XCloseDisplay(dpy);
    renderer_cleanup(&rend);
    profiler_stop(&prof);
    profiler_print(&prof);
    profiler_cleanup(&prof);
}

void check_collision_block(bool *collided) {
    *collided = false;
    for (int i = 0; i < BLOCK_NROW; i++) {
        for (int j = 0; j < BLOCK_NCOL; j++) {
            if (block_is_removed[i][j]) {
                continue;
            }
            if (RectIntersect(BALL_RECT, Rect(BLOCK_X(j), BLOCK_Y(i),
                                              BLOCK_WIDTH, BLOCK_HEIGHT))) {
                block_is_removed[i][j] = true;
                *collided = true;
            }
        }
    }
}

void update(void) {
    profiler_start(&prof, "update");
    if (key_cache['a']) {
        handle_x -= HANDLE_SPEED;
    }
    if (key_cache['d']) {
        handle_x += HANDLE_SPEED;
    }

    ball_x += ball_vx;
    bool handle_collide_x = false;
    bool block_collide;
    check_collision_block(&block_collide);
    if (block_collide || RectIntersect(HANDLE_RECT, BALL_RECT) > 0 ||
        ball_x >= WIDTH - BALL_SIZE || ball_x <= 0) {
        handle_collide_x = true;
        ball_vx *= -1;
        ball_x += ball_vx;
    }
    ball_y += ball_vy;
    check_collision_block(&block_collide);
    if (block_collide ||
        !handle_collide_x && RectIntersect(HANDLE_RECT, BALL_RECT) > 0 ||
        ball_y >= HEIGHT - BALL_SIZE || ball_y <= 0) {
        ball_vy *= -1;
        ball_y += ball_vy;
    }
    profiler_stop(&prof);
}

void frame(void) {
    profiler_start(&prof, "frame");
    renderer_fill_byte(&rend, 0);
    renderer_put_rect(&rend, ball_x, ball_y, BALL_SIZE, BALL_SIZE, 0xFFFFFF);
    renderer_put_rect(&rend, handle_x, handle_y, HANDLE_WIDTH, HANDLE_HEIGHT,
                      0xFFFF00);
    for (int i = 0; i < BLOCK_NROW; i++) {
        for (int j = 0; j < BLOCK_NCOL; j++) {
            if (block_is_removed[i][j]) {
                continue;
            }
            renderer_put_rect(&rend, BLOCK_X(j), BLOCK_Y(i), BLOCK_WIDTH,
                              BLOCK_HEIGHT, 0xFFFFFF);
        }
    }
    profiler_stop(&prof);
}
